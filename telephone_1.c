//Telephone number validation(IP7NEM)

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "telephone_1.h"

int checkEntry(int argc, char *argv[]){
    if(!argv){
        return EXIT_FAILURE;
    }
    if(argc<MIN){
        printf("No number provided.\n");                            //argc<2; no argument provided
        return EXIT_SUCCESS;
    }
    else if(argc>MIN){                                              //argc>2; too many argument provided
        printf("Please provide one number at a time.\n");
        return EXIT_SUCCESS;
    }
    else{                                                           //argc=2; only one argument provided(acceptable)
        printf("Number provided:%s\n",valid_number(argv[1]));
        return EXIT_SUCCESS;
    }
}


char* valid_number(char* str){
    if(!str || !*str){
        return NULL;
    }

    int i;

    for(i=0;str[i]!='\0';i++){
            if(str[i]<'0' || str[i]>'9'){
            printf("Telephone number is invalid\n");
            return 0;
        }
    }
            printf("Telephone number is valid\n");
             return str;

return EXIT_SUCCESS;
}
